<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            
            *{
                padding:0;
                margin:20;
            }

            div{
                background-color:#ccc;
                height: 150px;
                text-align: justify;
                border:black solid;
                width:70%;
              
            }

            #ejer1{
                background-color:whitesmoke;
                width:10%;
                height: 20px;
                border:thin black solid;
                margin-left: 30px;
                margin-bottom: 30px;
            }
            
            label{
                margin-left: 30px;
            }
            
            </style>
        </head>
        <body>
            <h1>EJERCICIO NUMERO 1 DEL EXAMEN DE PHP</h1>
            <br>
            <h4>Escriba un numero(0 < número) y dibujaré una tabla de una columna de ese tamaño con cajas de texto en cada celda.</h4>

            <form>
                <div>
                    <div id="ejer1">Ejercicio 1</div>
                    <br>
                    <label for="tabla">Tamaño de la tabla &nbsp;</label><input type="text" name="numero" placeholder="Introduce un número" required>
                    <br><br>
                    <button type="submit" name="dibujar" id="dibujar" value="submit">Dibujar</button>
                    <button type="reset" name="restablecer" id="restablecer" value="reset">Restablecer</button>
                </div>
            </form>
            <?php
            if (isset($_GET['dibujar'])) {
                echo "<form>";
                $cajas = $_GET['numero'];
                for ($c = 0; $c < $cajas; $c++) {
                    echo '<input type="text" name="numero[]">';
                }
                ?>
                <button type="submit" name="calcular" id="dibujar" value="submit">Calcular</button>
                <button type="reset" name="restablecer" id="restablecer" value="reset">Restablecer</button>
                <?php
                echo "</form>";
            }
            ?>

            <?php
            if (isset($_GET['calcular'])) {
                $numeros = $_GET['numero'];
                $repetidos = array_count_values($numeros);
                $hay = 0;
                foreach ($repetidos as $valor) {
                    if ($valor > 1) {
                        $hay = 1;
                    }
                }
                if ($hay == 1) {
                    echo "Hay elementos repetidos";
                } else {
                    echo "No hay elementos repetidos";
                }

                $rellenado = count($numeros);
                $vacios = 0;

                foreach ($numeros as $valor) {
                    if ($valor == "") {
                        $vacios++;
                    }
                }

                echo "<br>";
                echo 'Has rellenado ' . ($rellenado - $vacios) . ' cajas de un total de' . $rellenado;
            }
            ?>
        </body>
    </html>
